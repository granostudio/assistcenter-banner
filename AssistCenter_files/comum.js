
// menu

$( window ).scroll(function() {
	if($(this).scrollTop()>20){
		$('.navbar').addClass('active');
	} else {
		$('.navbar').removeClass('active');
	}
	
});

// menu end

//footer
jQuery(document).ready(function($) {
	jQuery(".modalFooterTermo").on("click", function(event) {
	    event.preventDefault();
	
	    abrirModal("#modalFooterTermo");
	});
	
	jQuery(".modalFooterPolitica").on("click", function(event) {
	    event.preventDefault();
	
	    abrirModal("#modalFooterPolitica");
	});
	
	jQuery('body').addClass('home');

	// TESTE DE FUNCIONAMENTO DE MODAL
	// $('#vejamaisBanner').on('click', function(event) {
	// 	event.preventDefault();
	// 	console.log("oi");
	// 	$(".panel-modal").toggleClass('modal-active');
	//   });
});
//Back to Top ===============================================
 $(window).scroll(function () {
     if ($(this).scrollTop() > 50) {
         $('#back-to-top').fadeIn();
     } else {
         $('#back-to-top').fadeOut();
         $('#back-to-top').tooltip('hide');
     }
 });
 // scroll body to 0px on click
 $('#back-to-top').click(function () {
     $('#back-to-top').tooltip('hide');
     $('body,html').animate({
         scrollTop: 0
     }, 800);
     return false;
 });
 $('#back-to-top').hover(function () {
     $('#back-to-top').tooltip('show');
 }, function () {
     $('#back-to-top').tooltip('hide');
 });

///Back to Top ===============================================
 
 //footer end


function startLoading(loading, button, buttonDisabled) {
	jQuery(button).addClass("hidden");
	jQuery(buttonDisabled).removeClass("hidden");
	jQuery(loading).removeClass("hidden");
}

function stopLoading(loading, button, buttonDisabled) {
	jQuery(loading).addClass("hidden");
	jQuery(buttonDisabled).addClass("hidden");
	jQuery(button).removeClass("hidden");
}

function removerErro() {
	jQuery('input.form-control').on('focus', function(event) {
		jQuery(event.target).parent().next('.erroValidacao').hide();
		jQuery(event.target).closest("form").find('.erroValidacao').hide();
	});
}

function temErro(idForm) {
	return jQuery('#' + idForm + ' .erroValidacao:visible').length != 0;
}
// abrir modal
function abrirModal(modal) {
	jQuery(modal).addClass('ativo');
	eventoFecharModal();
}

function eventoFecharModal() {
	jQuery('.btnFechar').on('click', function() {
		jQuery(".modal-box.ativo").removeClass('ativo');
	});
}

function mascaraTelefone() {
	var inputTelefone = jQuery("input.telefone");
	var telefone = inputTelefone.val().replace(/\D/g, '');
	
	if (telefone.length > 10) {
		inputTelefone.mask("(99) 99999-999?9");
	} else {
		inputTelefone.mask("(99) 9999-9999?9");
    }

	inputTelefone.focusout(function (event) {
	    var target, phone, element;
	    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
	    phone = target.value.replace(/\D/g, '');
	    element = $(target);
	    element.unmask();
	    
	    if (phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");
	    }
	});
}

//funcao com o load pq o ajax quebrou os eventos do js
function carregar() {
	removerErro();
	
	jQuery(".brand").hover(
			function(){//in
				jQuery(this).attr('src', jQuery(this).attr('src').replace('pb',''));
			},
			function(){//out
				var selectedImg = jQuery('[id$="hiddenRadio"]').find('input').index(jQuery('[id$="hiddenRadio"]').find('input:checked'));
				if(jQuery(this).attr('alt') != selectedImg.toString()){
					jQuery(this).attr('src', jQuery(this).attr('src').replace('.png','pb.png'));
				}
			}
	);
	
	//verifica se ja tem um radio selecionado e marca a img correspondente
	jQuery(".brand").click(function(){
		jQuery(".brand").each(function(){
			jQuery(this).attr('src', jQuery(this).attr('src').replace('pb',''));
			jQuery(this).attr('src', jQuery(this).attr('src').replace('.png','pb.png'));
		});
		var index = jQuery(this).attr('alt');
		jQuery('[id$="hiddenRadio:'+ index + '"]').click();
		jQuery(this).attr('src', jQuery(this).attr('src').replace('pb',''));
		
		jQuery('#formCadastroCartao .erroValidacao').hide();
	});
	
	var radio = jQuery('[id$="hiddenRadio"]').find('input:checked');
	if(jQuery(radio).length > 0){
		var imgIndex = jQuery('[id$="hiddenRadio"]').find('input').index(radio);
		//jQuery("[alt='" + imgIndex + "']").click();//o atributo alt de cada img guarda o index do selectitems
		jQuery("[alt='" + imgIndex + "']").attr('src', jQuery("[alt='" + imgIndex + "']").attr('src').replace('pb',''));
		
	}
	
	//workaround para o chrome android
	try {
		jQuery('[id$="numeroCartao"]').prop('type','tel');
		jQuery('[id$="dataValidade"]').prop('type','tel');
		jQuery('[id$="codigoSeguranca"]').prop('type','tel');
		jQuery('#cpf').prop('type','tel');
		jQuery('#telefone').prop('type','tel');
		jQuery('#dataNascimento').prop('type','tel');
		jQuery('#cep').prop('type','tel');
		jQuery('#numero').prop('type','tel');
	} catch (exception) {}
}

function checkNumerico(seletor) {
	jQuery(seletor).val(jQuery(seletor).val().replace(/\D/g, ""));
}
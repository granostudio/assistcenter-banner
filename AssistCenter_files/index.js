jQuery(document).ready(function($) {

  // banner home
  $(document).ready(function(){
	  if ($('.bxslider').length > 0) {
	    $('.bxslider').bxSlider({
	      pagerCustom: '#bx-pager',
	      controls: false,
	      auto: true,
        autoHover: true,
        touchEnabled: false
	    });
	  }
	  if ($('.bxslider2').length > 0) {
	    $('.bxslider2').bxSlider({
	      pagerCustom: '#bx-pager2',
	      controls: false,
	      auto: true,
        autoHover: true,
        touchEnabled: false
	    });
	  }
  });

  	//scroll to
    $("a.scrollto").click(function(e) {
          // Prevent a page reload when a link is pressed
        e.preventDefault();
          // Call the scroll function
        goToByScroll(this.id);
    });
  
  	jQuery("a.adquirir").click(function(e) {
      // Prevent a page reload when a link is pressed
  		e.preventDefault();
      // Configura o id do produto escolhido
  		jQuery("#idProduto").val(jQuery(this).attr("id-produto"));
  		// Envia o produto escolhido na loja.shtml
  		jQuery("#enviarProduto, #enviarParaLogin").click();
  	});

    // abrir modal
  	/*
    function abrirModal(modal){
      $(modal).addClass('ativo');
      $(modal).find('.btnFechar').on('click', function() {
        $(modal).removeClass('ativo');
      });
    }*/
  	
  	$('.casaMensal').on('click', function(event) {
        event.preventDefault();

        abrirModal('.modal-casa-mensal');
    });

    $('.casaAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-casa-anual');
    });

    $('.casaSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-casa-semestral');
    });
  	
  	$('.autoMensal').on('click', function(event) {
        event.preventDefault();
        abrirModal('.modal-auto-mensal');
    });

    $('.autoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-auto-anual');
    });

    $('.autoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-auto-semestral');
    });
    
    $('.motoMensal').on('click', function(event) {
        event.preventDefault();

        abrirModal('.modal-moto-mensal');
    });

    $('.motoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-moto-anual');
    });

    $('.motoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-moto-semestral');
    });
    
    $('.caminhaoMensal').on('click', function(event) {
        event.preventDefault();

        abrirModal('.modal-caminhao-mensal');
    });
    
    $('.caminhaoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-caminhao-anual');
    });

    $('.caminhaoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-caminhao-semestral');
    });
    
    // banner
    $('.veja-mais').on('click', function(event) {
      event.preventDefault();
      $('.panel-helpdesk').addClass('active');
    });
    $('#closeBanner').on('click', function(event) {
      event.preventDefault();
      $('.panel-helpdesk').removeClass('active');
    });
    $(".pontos .p").on("mouseover", function(event){
      event.preventDefault();
      var mask = $(this).data('mask');
      $(mask).addClass('active');
    });
    $(".pontos .p").on("mouseout", function(event){
      event.preventDefault();
      var mask = $(this).data('mask');
      $(mask).removeClass('active');
    });

    
    // submit form com enter
    $("#cpf, #password").keypress(function (event) {
    	if (event.which == 13) {
    		$("#btnEntrar").click();
    	}
	});
    
    //workaround para o chrome android
	try {
		jQuery('#cpf').prop('type','tel');
  } catch (exception) {};
  
  
});

function goToByScroll(id) {
	// Remove "link" from the ID
	id = id.replace("link", ""),
	headerH = $('.header-top').height();
	
	if ($("#"+id).length > 0) {
		// Scroll
		$('html,body').animate({
			scrollTop: ($("#"+id).offset().top-headerH-40)}, 'slow');
	}
}

